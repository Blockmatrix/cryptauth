import merkletools
from datetime import datetime
from hashlib import sha256
import qrcode
from PIL import ImageDraw, ImageFont
import os

FONT = ImageFont.truetype(os.path.join(os.path.split(__file__)[0], "fonts/DejaVuSansMono.ttf"), 12)


def hashit(d: object) -> str:
    try:
        h = sha256(d).hexdigest()
    except TypeError as e:
        h = sha256(bytes(str(d), encoding='utf8')).hexdigest()
    return h


class Seal:
    def __init__(self, name: str, content: bytes, start: datetime, **metadata) -> object:
        """
        Authentication Seal class. Builds a Merkle tree with the data.
        :param name: Name of the document, or short descriptive string.
        :param content: Hash hexdigest of digital object to be authenticated
        :param start: Date of the authentication.
        :param end: Expiration date
        :param location: (lat,lon) tuple with geographical coordinates associated to the object
        :param metadata: extra metadata to be included in the Merkle tree
        """
        self.name = name
        self.content_hash = content
        self.start = start
        self.metadata = metadata
        self.merkle_tree = merkletools.MerkleTools(hash_type='sha256')
        self.ID = self._build_tree()
        self.payload = {'name': name, 'content': content, 'start': start}
        self.payload.update(metadata)

    def _build_tree(self) -> object:
        md_leaves = [hashit(md) for md in self.metadata.items()]
        md_leaves.sort()  # to make metadata addition commutative, i.e., order does not matter.
        self.merkle_tree.add_leaf([self.name,
                                   self.content_hash,
                                   self.start.isoformat(),
                                   ] + md_leaves, do_hash=True)
        self.merkle_tree.make_tree()
        assert self.merkle_tree.is_ready
        return self.merkle_tree.get_merkle_root()



    @property
    def qr_code(self) -> object:
        """
        QR-Code representing the Merkle root
        :return:
        """
        if not self.merkle_tree.is_ready:
            return
        img = qrcode.make(self.ID)
        draw = ImageDraw.Draw(img)

        draw.text((4, 4), 'Authenticha.in {}'.format(self.start.isoformat()), fill=None, font=FONT)
        img.save('testQR.png', 'PNG')

        return img
