"""
Publishing tools for blockchains and Peer2peer file systems
"""
import logging
import ipfshttpclient
import web3
from web3 import Web3, HTTPProvider
from web3.providers.eth_tester import EthereumTesterProvider
from io import BytesIO
import os
import dotenv

dotenv.load_dotenv()


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
ch = logging.FileHandler('publish.log')
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

try:
    #W3 = Web3()# Autodetects Provider
    W3 = Web3(Web3.HTTPProvider(f'https://mainnet.infura.io/v3/{os.getenv("INFURA_KEY")}'))
    assert W3.isConnected()
    ETH = True
except (ConnectionError, AssertionError) as e:
#    W3 = Web3(EthereumTesterProvider())
    logger.warning('Could not connect to infura: {}'.format(e))
    ETH = False


def publish_ipfs(doc: str, host: str='127.0.0.1', port: int=5001, JSON: bool=True) -> dict:
    """
    Publishes string to the IPFS, connecting to a running node on `host`
    :param port: port to use in the IPFS server to use.
    :param doc: string to be published: merkle-tree root.
    :param host: host running a node of IPFS
    :return:
    """
    api = ipfshttpclient.connect()
    if JSON:
        res = api.add_json({'ID': doc})
    else:
        f = BytesIO(doc.encode('utf8'))
        res = api.add(f)
    return res


def publish_ethereum(mtroot: str, From: str="", value=0) -> str:
    """
    Store the merkle tree root as data into an ethereum transaction.
    :param mtroot: Merkle tree root to be published
    :param From: Address paying for the publication
    :param value: value to be sent
    :return: Tx ID
    """
    try:
        int(mtroot, 16)
    except ValueError:
        logger.error("transaction is not valid hexadecimal.\nNot publishing.")
        return
    try:
        nonce = W3.eth.get_transaction_count(From)
    except web3.exceptions.InvalidTransaction as e:
        logger.error("Could not fetch nonce: {}".format(e))
        nonce = 0
    transaction = {
        'from': From,
        'to': '0xf17f52151EbEF6C7334FAD080c5704D77216b732',
        'data': bytes(mtroot, encoding='ascii'),
        'value': value,
        'gas': 100000,
        'gasPrice': 0,  # W3.eth.gasPrice,
        'nonce': nonce
    }
    if ETH:
        signed_txn = W3.eth.account.signTransaction(transaction, private_key=os.environ['PRIVATE_KEY'])
        txid = W3.toHex(W3.eth.sendRawTransaction(signed_txn.rawTransaction))
    else:
        txid = '0x0'
    return txid
