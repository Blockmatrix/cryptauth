from cryptauth.authenticate import Seal
from cryptauth.publish import publish_ipfs, publish_ethereum, W3
from hashlib import sha256
from datetime import datetime
import requests

document = b"this is a test document"
content = sha256(document).hexdigest()
now = datetime.now()

eth_from = '0x0c9D26Ce9F51CcA439B2Ab93e18D18EB73BBdb6E'

def test_IPFS_publish_JSON():
    S = Seal("test object", content, now, x=1, y=7)
    pub = publish_ipfs(S.ID)
    assert isinstance(pub, str)

def test_IPFS_publish():
    with open('tests/fixtures/testpage.html') as f:
        doc = f.read()
    pub = publish_ipfs(doc, JSON=False)
    p = requests.get('https://ipfs.io/ipfs/{}'.format(pub['Hash']))
    assert doc.encode() == p.content


def test_IPFS_publicating_different_docs_yields_different_hashes():
    doc1 = sha256(b'lalala').hexdigest()
    doc2 = sha256(b'lilali').hexdigest()
    res1 = publish_ipfs(doc1)
    res2 = publish_ipfs(doc2)
    assert doc1 != doc2
    assert res1 != res2
    
def test_ethereum_connection():
    blockn = W3.eth.blockNumber
    assert isinstance(blockn, int)
    

def test_ethereum_publish():
    S = Seal("test object", content, now, x=1, y=7)
    txid = publish_ethereum(S.ID, From=eth_from, value=0)
    assert txid != '0x0'
