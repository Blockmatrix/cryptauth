#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest
from cryptauth.authenticate import Seal
from cryptauth.publish import publish_ipfs
from hashlib import sha256
from datetime import datetime
from qrcode.image.pil import PilImage

__author__ = "Flavio C. Coelho"
__copyright__ = "Flavio C. Coelho"
__license__ = "mit"


document = b"this is a test document"
content = sha256(document).hexdigest()
now = datetime.now()




def test_ID():
    S = Seal("test object", content, now, x=1, y=7)
    assert isinstance(S.ID, str)

def test_comutativeness_of_leaves():
    S = Seal("test object", content, now, x=1, y=7)
    T = Seal("test object", content, now, y=7, x=1)
    assert S.ID == T.ID

def test_uniqueness():
    S = Seal("test object", content, now, x=1, y=7)
    T = Seal("test object 2", content, now, x=7, y=1)
    assert S.ID != T.ID

def test_payload():
    S = Seal("test object", content, now, x=1, y=7)
    assert 'x' in S.payload
    assert 'y' in S.payload


def test_qr_code():
    S = Seal("test object", content, now, x=1, y=7)
    img = S.qr_code
    img.save('testQR.png', 'PNG')
    assert isinstance(img, PilImage)

